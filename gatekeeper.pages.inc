<?php

function gatekeeper_code_generation_page($account) {
  $output = ''; 
  drupal_set_title(t(
            'Verify identity of @username', 
            array('@username' => $account->name)
  ));
  
  if (gatekeeper_is_validated($account->uid)) {
    return '<p>'.t('This account has already been validated.').'</p>';
  }
  
  if (isset($_SESSION['gatekeeper_codes'][$account->uid])) {
   
    //TODO make this themeable
    $output .= '<h3>'.t('Validation Code').'</h3>'
            .'<p>'.$_SESSION['gatekeeper_codes'][$account->uid].'</p>'
            .'<p>'.t('Give this code to the person named below. '
                    .'They need to log in to the site and enter this code to gain full '
                    .'membership.')
            .'</p>';
    unset($_SESSION['gatekeeper_codes'][$account->uid]);
  }
  else {
    $output .= '<p>'.t('Check that the details for the person named below are correct. ')
            .t('Click "Get code" to get a code to give to this person. ')
            .t('They can use this code to validate their account and get full access to the site.')
            .'</p>';

    //Check whether user already has code
    if (gatekeeper_user_code_exists($account->uid)) {
      $output .= '<p>'.t(
              'If you generate a new code, the previous code will no longer be valid.'
      ).'</p>';
    }
    $output .= drupal_render(drupal_get_form('gatekeeper_code_generation_form', $account));
  }
  
  $output .= drupal_render(user_view($account));
  return $output;
}

function gatekeeper_code_generation_form($form, &$form_state, $account) {
  $form = array(
      'uid' => array(
          '#type' => 'hidden',
          '#access' => FALSE,
          '#default_value' => $account->uid,
      ),
      'submit' => array(
          '#type' => 'submit',
          '#value' => t('Get code'),
      ),
  );
  return $form;
}

function gatekeeper_code_generation_form_submit($form, &$form_state) {
  $code = user_password();
  $record = array(
      'uid' => $form_state['values']['uid'],
      'code' => sha1($code),
  );
  
  //check if user already has a code, in which case we are replacing it
  $primary_keys = array();
  if (gatekeeper_user_code_exists($form_state['values']['uid'])) {
    $primary_keys[] = 'uid';
  }
  
  $success = drupal_write_record('gatekeeper', $record, $primary_keys);
  if ($success) {
    $_SESSION['gatekeeper_codes'][$form_state['values']['uid']] = $code;
  }
}

function gatekeeper_validate_page() {
  
  $output = '';
  //TODO only show form if they have a code.  If no code, explain how to get one.
  //TODO this text should be editable in the admin form.
  $default_text = '<p>'.t('Enter your validation code to upgrade your account.').'</p>';
  $text = variable_get('gatekeeper_validation_text', $default_text);
  $output .= $text;
  $output .= drupal_render(drupal_get_form('gatekeeper_validation_form'));
  
  return $output;
}

function gatekeeper_validation_form($form, &$form_state) {
  $form = array(
      'code' => array(
          '#type' => 'textfield',
          '#title' => t('Your validation code'),
          '#required' => TRUE,
      ),
      'submit' => array(
          '#type' => 'submit',
          '#value' => t('Validate'),
      )
  );
  return $form;
}

function gatekeeper_validation_form_validate($form, &$form_state) {
  //check that the validation code is correct for current user
  global $user;
  $sql = 'SELECT COUNT(*) AS count FROM {gatekeeper} WHERE {gatekeeper}.uid = :uid '
         .'AND {gatekeeper}.code = :code AND {gatekeeper}.validated = 0';
  $code = sha1(trim($form_state['values']['code']));
  $result = db_query($sql, array(':uid' => $user->uid, ':code' => $code));
  while ($row = $result->fetchAssoc()) {
    if ((int) $row['count'] < 1) {
      form_set_error('code', t('Sorry, that\'s not the right code.'));
    }
  }
}

function gatekeeper_validation_form_submit($form, &$form_state) {
    global $user;
    //Fire rules event
    rules_invoke_event('gatekeeper_event_validate', $user);
    //role assignment, redirection and message are all configurable by rules,
    //so I just want to create a default ruleset for those rather than 
    //hard-coding theme here.  TODO - create code for this default rule
    
    //indicate that account has been validated
    $record = array(
        'validated' => 1,
        'uid' => $user->uid,
    );
    drupal_write_record('gatekeeper', $record, array('uid'));
}
