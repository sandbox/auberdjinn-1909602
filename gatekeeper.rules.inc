<?php

/**
 * Implements hook_rules_event_info().
 */
function gatekeeper_rules_event_info() {
  $events = array();

  $events['gatekeeper_event_validate'] = array(
      'label' => t('A user validates their account'),
      'group' => t('Gatekeeper'),
      'variables' => array(
         'user' => array(
            'type' => 'user',
            'label' => t('The user who validated their account.'),
        ),
      )
  );

  return $events;
}